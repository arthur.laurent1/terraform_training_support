variable "vpc_name" {
  type        = string
  description = "The name of my custom vpc network"
  default     = "custom-vpc"
}
