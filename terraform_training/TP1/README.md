# TP1
## Deploy resources with Terraform
* Create file architecture
* Add provider
* Add vpc
* Perform terraform init
* Perform terraform apply
* Check new changes and enter ‘yes’
* See the result in gcp console
* Create subnet and a VM
* Perform terraform apply
* In compute engine console you should see a new instance which use your custom_vpc and custom_subnet
